package com.tasks;

import java.util.*;

public class Task implements Runnable {

    private final int number;

    public Task(int n) {
        number = n;
    }

    public void run() {
        System.out.printf("Task #%d is running by (%S)\n", number, Thread.currentThread().getName());
        List<Integer> list = new ArrayList<>();
        for (long i = 0; i < 3_000_000; i++) {
            list.add(new Random().nextInt(1000));
        }
        Collections.sort(list);
        System.out.printf("Task #%d is completed by (%S)\n", number, Thread.currentThread().getName());
    }
}
