package com.threadPool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool implements ThreadPool {

    private final int countThreads;
    private final List<Worker> threads;
    private final LinkedBlockingQueue<Runnable> queue;

    private volatile boolean isRunning = true;

    public FixedThreadPool(int countThreads) {
        this.countThreads = countThreads;
        this.queue = new LinkedBlockingQueue<>();
        this.threads = new ArrayList<>(countThreads);
    }

    public void execute(Runnable task) {
        synchronized (queue) {
            queue.add(task);
            queue.notify();
        }
    }

    public void start() {
        for (int i = 0; i < countThreads; i++) {
            threads.add(new Worker());
            threads.get(i).start();
        }
    }

    public void shutdown(){
        isRunning = false;
    }

    private class Worker extends Thread {
        public void run() {
            Runnable task;

            while (isRunning) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            System.err.println("Ошибка во время ожидания потока: " + e.getMessage());
                        }
                    }
                    task = queue.poll();
                }

                try {
                    task.run();
                } catch (RuntimeException e) {
                    System.err.println("Пул потоков прерван: " + e.getMessage());
                }
            }
        }
    }

}
