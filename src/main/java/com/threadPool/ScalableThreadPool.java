package com.threadPool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class ScalableThreadPool implements ThreadPool {

    public int minCountThreads;
    public int maxCountThreads;

    private final List<Worker> threads;
    private final LinkedBlockingQueue<Runnable> queue;

    private volatile boolean isRunning = true;

    public ScalableThreadPool(int minCountThreads, int maxCountThreads) {
        this.minCountThreads = minCountThreads;
        this.maxCountThreads = maxCountThreads;

        this.queue = new LinkedBlockingQueue<>();
        this.threads = new ArrayList<>(minCountThreads);
    }

    public void start() {
        for (int i = 0; i < minCountThreads; i++) {
            threads.add(new Worker());
            threads.get(i).start();
        }
    }

    public void shutdown() {
        isRunning = false;
    }

    private void removeWorker(Worker worker) {
        synchronized (queue) {
            if (queue.size() == 0 && threads.size() > minCountThreads) {
                worker.dismiss();
                threads.remove(worker);
            }
        }
    }

    public void execute(Runnable task) {
        synchronized (queue) {
            queue.add(task);

            for (Worker thread : threads) {
                if (thread.getState() == Thread.State.WAITING) {
                    queue.notify();
                    return;
                }
            }

            if (threads.size() < maxCountThreads) {
                threads.add(new Worker());
                threads.get(threads.size() - 1).start();
            }

            queue.notify();
        }
    }

    private class Worker extends Thread {

        private volatile boolean canWork = true;

        public void dismiss() {
            canWork = false;
        }

        public void run() {
            Runnable task;

            while (isRunning) {
                while (canWork) {
                    synchronized (queue) {
                        while (queue.isEmpty()) {
                            try {
                                queue.wait();
                            } catch (InterruptedException e) {
                                System.err.println("Ошибка во время ожидания потока: " + e.getMessage());
                            }
                        }
                        task = queue.poll();
                    }

                    try {
                        task.run();
                    } catch (RuntimeException e) {
                        System.err.println("Пул потоков прерван: " + e.getMessage());
                    }

                    removeWorker(this);
                }
            }
        }
    }
}
