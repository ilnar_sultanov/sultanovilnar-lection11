package com;

import com.tasks.Task;
import com.threadPool.FixedThreadPool;
import com.threadPool.ScalableThreadPool;
import com.threadPool.ThreadPool;

public class Main {

    private static final int NUMBER_OF_TASKS = 100;

    public static void main(String[] args) {
        try {
            System.out.println("*** FixedThreadPool ***\n");
            ThreadPool fixedPool = new FixedThreadPool(16);
            fixedPool.start();

            int taskNumber;
            for (taskNumber = 1; taskNumber <= NUMBER_OF_TASKS; taskNumber++) {
                Task task = new Task(taskNumber);
                fixedPool.execute(task);
            }

            // fixedPool shutdown after 20 sec
            Thread.sleep(20_000);
            fixedPool.shutdown();

            System.out.println("\n*** ScalableThreadPool ***\n");
            ThreadPool scalablePool = new ScalableThreadPool(4, 16);
            scalablePool.start();
            int taskNumber2;
            for (taskNumber2 = 1; taskNumber2 <= NUMBER_OF_TASKS; taskNumber2++) {
                Task task = new Task(taskNumber2);
                scalablePool.execute(task);
            }

            // scalablePool shutdown after 20 sec
            Thread.sleep(20_000);
            scalablePool.shutdown();
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
